import React, {Component} from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Artists from "./containers/Artists/Artists";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import History from "./containers/History/History";
import Layout from "./containers/Layout/Layout";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Artists}/>
                    <Route path="/artist/:id" exact component={Albums}/>
                    <Route path="/artist/:id/album/:albumID" exact component={Tracks}/>
                    <Route path="/history" exact component={History}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
