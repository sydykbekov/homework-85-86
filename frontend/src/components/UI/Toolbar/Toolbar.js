import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Toolbar = ({user}) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>Last.fm</a></LinkContainer>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav pullRight>
                {user && <NavItem>Hello, {user.username}!</NavItem>}
                <LinkContainer to="/" exact>
                    <NavItem>Artists</NavItem>
                </LinkContainer>
                <LinkContainer to="/history" exact>
                    <NavItem>History</NavItem>
                </LinkContainer>
                <LinkContainer to="/register" exact>
                    <NavItem>Sign Up</NavItem>
                </LinkContainer>
                <LinkContainer to="/login" exact>
                    <NavItem>Login</NavItem>
                </LinkContainer>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;