import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import ArtistList from "../../components/ArtistList/ArtistList";
import {PageHeader} from "react-bootstrap";
import {getArtists} from "../../store/actions/artists";

class Artists extends Component {
    componentDidMount() {
        this.props.getArtists();
    }
    render() {
        return (
            <Fragment>
                <PageHeader>
                    Исполнители
                </PageHeader>
                {this.props.artists.map(artist => (
                    <ArtistList key={artist._id} id={artist._id} name={artist.name} image={artist.image} />
                ))}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
    getArtists: () => dispatch(getArtists())
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);
