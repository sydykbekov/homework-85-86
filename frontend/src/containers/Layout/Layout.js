import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import Toolbar from "../../components/UI/Toolbar/Toolbar";

const Layout = ({user, children}) => (
    <Fragment>
        <header>
            <Toolbar user={user} />
        </header>
        <main className="container">
            {children}
        </main>
    </Fragment>
);

const mapStateToProps = state => ({
    user: state.users.user
});

export default connect(mapStateToProps)(Layout);