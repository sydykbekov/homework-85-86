import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider}from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import {routerMiddleware, routerReducer, ConnectedRouter} from 'react-router-redux';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import artistsReducer from './store/reducers/artists';
import usersReducer from './store/reducers/users';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:8000/';

const rootReducer = combineReducers({
    artists: artistsReducer,
    users: usersReducer,
    routing: routerReducer
});

const history = createHistory();

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const store = createStore(rootReducer, enhancers);

const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
