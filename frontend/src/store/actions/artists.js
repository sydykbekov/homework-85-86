import axios from 'axios';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';
export const SUCCESS_FETCH_ALBUMS = 'SUCCESS_FETCH_ALBUMS';
export const SUCCESS_FETCH_TRACKS = 'SUCCESS_FETCH_TRACKS';
export const SUCCESS_FETCH_HISTORY = 'SUCCESS_FETCH_HISTORY';

const successRequest = (artists) => {
    return {type: SUCCESS_REQUEST, artists};
};

const successFetchAlbums = (albums) => {
    return {type: SUCCESS_FETCH_ALBUMS, albums};
};

const successFetchTracks = (tracks) => {
    return {type: SUCCESS_FETCH_TRACKS, tracks};
};

const successFetchHistory = (history) => {
    return {type: SUCCESS_FETCH_HISTORY, history};
};

export const getArtists = () => {
    return dispatch => {
        axios.get('artists').then(response => {
            dispatch(successRequest(response.data));
        });
    };
};

export const getAlbums = (id) => {
    return dispatch => {
        axios.get(`albums?artist=${id}`).then(response => {
            dispatch(successFetchAlbums(response.data));
        });
    };
};

export const getTracks = (id) => {
    return dispatch => {
        axios.get(`tracks?album=${id}`).then(response => {
            dispatch(successFetchTracks(response.data));
        });
    };
};

export const getHistory = (id) => {
    return dispatch => {
        axios.get(`track_history?userID=${id}`).then(response => {
            dispatch(successFetchHistory(response.data));
        });
    };
};

export const sendHistory = (listenedTrack, token) => {
    return dispatch => {
        axios.post('track_history', listenedTrack, {
            headers: {
                'Token': token
            }
        });
    };
};
